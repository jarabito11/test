<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/
/*
Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});*/

//PREFIJO API.
Route::group(['prefix' => 'auth'], function () {

    Route::post('login', 'ApiAuthController@index');

    //USERS LOGGUED
    Route::group(['middleware' => 'auth:api'], function() {

        Route::get('logout', 'AuthController@logout');
        Route::get('user', 'AuthController@user');

        //OBTENER DISPOSITIVOS
        Route::get('devices', 'ApiAuthController@getDevices');


        //OBTENER REPORTES
        Route::get('reports', 'API\ReportController@getReports');
        Route::get('reportId', 'API\ReportController@getReportId');

        //CREAR REPORTES
        Route::post('newReport', 'API\ReportController@postNewReport');

    });

});
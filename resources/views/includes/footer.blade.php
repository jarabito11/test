<footer class="pb-4 bg-primary-3 text-light" id="footer">
    <div class="container">
        <div class="row mb-5">
            <div class="col-6 col-lg-3 col-xl-2">
                <h5>Navigate</h5>
                <ul class="nav flex-column">
                    <li class="nav-item">
                        <a href="#" class="nav-link">Demos</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">Pages</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">Blog</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">Portfolio</a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">Elements</a>
                    </li>
                </ul>
            </div>
            <div class="col-6 col-lg">
                <h5>Contact</h5>
                <ul class="list-unstyled">
                    <li class="mb-3 d-flex">
                        <img class="icon" src="/assets/img/icons/theme/map/marker-1.svg" alt="marker-1 icon" data-inject-svg />
                        <div class="ml-3">
                  <span>348 Greenpoint Avenue
                    <br />Brooklyn, NY</span>
                        </div>
                    </li>
                    <li class="mb-3 d-flex">
                        <img class="icon" src="/assets/img/icons/theme/communication/call-1.svg" alt="call-1 icon" data-inject-svg />
                        <div class="ml-3">
                            <span>+61 3928 3324</span>
                            <span class="d-block text-muted text-small">Mon - Fri 9am - 5pm</span>
                        </div>
                    </li>
                    <li class="mb-3 d-flex">
                        <img class="icon" src="/assets/img/icons/theme/communication/mail.svg" alt="mail icon" data-inject-svg />
                        <div class="ml-3">
                            <a href="#">hello@company.io</a>
                        </div>
                    </li>
                </ul>
            </div>
            <div class="col-lg-6 col-md-8 justify-content-center text-center" data-aos="fade-up">
                <h3 class="display-4">Download Flubber.</h3>
                <div class="d-sm-flex justify-content-center my-2 my-md-4">
                    <a href="#" class="btn btn-lg btn-primary mx-sm-2 mb-3 mb-sm-0">
                        <img class="icon" src="/assets/img/icons/social/apple.svg" alt="apple social icon" data-inject-svg />
                        <span>App Store</span>
                    </a>
                    <a href="#" class="btn btn-lg btn-primary mx-sm-2 mb-3 mb-sm-0">
                        <img class="icon" src="/assets/img/icons/social/googleplay.svg" alt="googleplay social icon" data-inject-svg />
                        <span>Google Play</span>
                    </a>
                </div>
                <div class="mx-lg-5 text-small text-muted">Add some helper text here to explain the finer details of your product or service.</div>
            </div>
        </div>
        <div class="row justify-content-center mb-2">
            <div class="col-auto">
                <ul class="nav">
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <img class="icon undefined" src="/assets/img/icons/social/instagram.svg" alt="instagram social icon" data-inject-svg />
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <img class="icon undefined" src="/assets/img/icons/social/twitter.svg" alt="twitter social icon" data-inject-svg />
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <img class="icon undefined" src="/assets/img/icons/social/youtube.svg" alt="youtube social icon" data-inject-svg />
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <img class="icon undefined" src="/assets/img/icons/social/medium.svg" alt="medium social icon" data-inject-svg />
                        </a>
                    </li>
                    <li class="nav-item">
                        <a href="#" class="nav-link">
                            <img class="icon undefined" src="/assets/img/icons/social/facebook.svg" alt="facebook social icon" data-inject-svg />
                        </a>
                    </li>
                </ul>
            </div>
        </div>
        <div class="row justify-content-center">
            <div class="col col-md-auto text-center">
                <small class="text-muted">&copy;2019 This page is protected by reCAPTCHA and is subject to the Google <a href="https://www.google.com/policies/privacy/">Privacy Policy</a> and <a href="https://policies.google.com/terms">Terms of Service.</a>
                </small>
            </div>
        </div>
    </div>
</footer>
<a href="#" class="btn back-to-top btn-primary btn-round" data-smooth-scroll data-aos="fade-up" data-aos-offset="2000" data-aos-mirror="true" data-aos-once="false">
    <img class="icon" src="/assets/img/icons/theme/navigation/arrow-up.svg" alt="arrow-up icon" data-inject-svg />
</a>
<!-- Required vendor scripts (Do not remove) -->
<script type="text/javascript" src="/assets/js/jquery.min.js"></script>
<script type="text/javascript" src="/assets/js/popper.min.js"></script>
<script type="text/javascript" src="/assets/js/bootstrap.js"></script>

<!-- Optional Vendor Scripts (Remove the plugin script here and comment initializer script out of index.js if site does not use that feature) -->

<!-- AOS (Animate On Scroll - animates elements into view while scrolling down) -->
<script type="text/javascript" src="/assets/js/aos.js"></script>
<!-- Clipboard (copies content from browser into OS clipboard) -->
<script type="text/javascript" src="/assets/js/clipboard.js"></script>
<!-- Fancybox (handles image and video lightbox and galleries) -->
<script type="text/javascript" src="/assets/js/jquery.fancybox.min.js"></script>
<!-- Flatpickr (calendar/date/time picker UI) -->
<script type="text/javascript" src="/assets/js/flatpickr.min.js"></script>
<!-- Flickity (handles touch enabled carousels and sliders) -->
<script type="text/javascript" src="/assets/js/flickity.pkgd.min.js"></script>
<!-- Ion rangeSlider (flexible and pretty range slider elements) -->
<script type="text/javascript" src="/assets/js/ion.rangeSlider.min.js"></script>
<!-- Isotope (masonry layouts and filtering) -->
<script type="text/javascript" src="/assets/js/isotope.pkgd.min.js"></script>
<!-- jarallax (parallax effect and video backgrounds) -->
<script type="text/javascript" src="/assets/js/jarallax.min.js"></script>
<script type="text/javascript" src="/assets/js/jarallax-video.min.js"></script>
<script type="text/javascript" src="/assets/js/jarallax-element.min.js"></script>
<!-- jQuery Countdown (displays countdown text to a specified date) -->
<script type="text/javascript" src="/assets/js/jquery.countdown.min.js"></script>
<!-- jQuery smartWizard facilitates steppable wizard content -->
<script type="text/javascript" src="/assets/js/jquery.smartWizard.min.js"></script>
<!-- Plyr (unified player for Video, Audio, Vimeo and Youtube) -->
<script type="text/javascript" src="/assets/js/plyr.polyfilled.min.js"></script>
<!-- Prism (displays formatted code boxes) -->
<script type="text/javascript" src="/assets/js/prism.js"></script>
<!-- ScrollMonitor (manages events for elements scrolling in and out of view) -->
<script type="text/javascript" src="/assets/js/scrollMonitor.js"></script>
<!-- Smooth scroll (animation to links in-page)-->
<script type="text/javascript" src="/assets/js/smooth-scroll.polyfills.min.js"></script>
<!-- SVGInjector (replaces img tags with SVG code to allow easy inclusion of SVGs with the benefit of inheriting colors and styles)-->
<script type="text/javascript" src="/assets/js/svg-injector.umd.production.js"></script>
<!-- TwitterFetcher (displays a feed of tweets from a specified account)-->
<script type="text/javascript" src="/assets/js/twitterFetcher_min.js"></script>
<!-- Typed text (animated typing effect)-->
<script type="text/javascript" src="/assets/js/typed.min.js"></script>
<!-- Required theme scripts (Do not remove) -->
<script type="text/javascript" src="/assets/js/theme.js"></script>
<!-- Removes page load animation when window is finished loading -->
<script type="text/javascript">
    window.addEventListener("load", function () {    document.querySelector('body').classList.add('loaded');  });
</script>
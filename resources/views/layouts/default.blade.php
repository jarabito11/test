<!doctype html>
<html lang="en">

<head>
    @include('includes.head')
</head>

<body data-smooth-scroll-offset="73">
    <div class="loader">
        <div class="loading-animation"></div>
    </div>

    @include('includes.menu')

    @yield('content')

    @include('includes.footer')

</body>

</html>
@extends('layouts.default')

@section('content')
    <div data-overlay class="bg-primary text-light o-hidden position-relative">
        <div class="position-absolute w-100 h-100 o-hidden top-0">
            <div class="decoration right bottom scale-2">
                <img class="bg-primary-2" src="/assets/img/decorations/deco-blob-2.svg" alt="deco-blob-2 decoration" data-inject-svg />
            </div>
            <div class="decoration right bottom scale-3">
                <img class="bg-white" src="/assets/img/decorations/deco-dots-6.svg" alt="deco-dots-6 decoration" data-inject-svg />
            </div>
            <div class="decoration top left scale-2  d-none d-md-block">
                <img class="bg-primary-3" src="/assets/img/decorations/deco-blob-1.svg" alt="deco-blob-1 decoration" data-inject-svg />
            </div>
        </div>
        <section class="min-vh-70 o-hidden d-flex flex-column justify-content-center">
            <div class="container">

            </div>
        </section>
        <div class="divider flip-x">
            <img src="/assets/img/dividers/divider-2.svg" alt="graphical divider" data-inject-svg />
        </div>
    </div>

    <section class="pt-5">
        <div class="container">
            <div class="row" data-isotope-collection data-isotope-id="projects">

                @foreach ($componentTested as $tested)
                    <div class="col-sm-6 col-lg-4 mb-4" data-isotope-item data-category="Digital">
                        <div class="card card-icon-3 card-body justify-content-between">
                            <div class="icon-round mb-3 mb-md-4 bg-primary">
                                <img class="icon bg-primary" src="/assets/img/icons/theme/devices/airpods.svg" alt="icon" data-inject-svg />
                            </div>
                            <span class="badge badge-primary">Badge Text</span>
                            <div>
                                <h3>{{ $tested->component->type }}</h3>
                                <p class="lead">
                                    @if(!$tested->work)
                                        No funciona
                                        @else
                                        Funciona!
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                @endforeach

            </div>
        </div>
    </section>
@endsection

{{--iconos de los componentes--}}


@extends('layouts.default')

@section('content')

    <div data-overlay class="bg-primary text-light o-hidden position-relative">
        <div class="position-absolute w-100 h-100 o-hidden top-0">
            <div class="decoration right bottom scale-2">
                <img class="bg-primary-2" src="/assets/img/decorations/deco-blob-2.svg" alt="deco-blob-2 decoration" data-inject-svg/>
            </div>
            <div class="decoration right bottom scale-3">
                <img class="bg-white" src="/assets/img/decorations/deco-dots-6.svg" alt="deco-dots-6 decoration" data-inject-svg/>
            </div>
            <div class="decoration top left scale-2  d-none d-md-block">
                <img class="bg-primary-3" src="/assets/img/decorations/deco-blob-1.svg" alt="deco-blob-1 decoration" data-inject-svg/>
            </div>
        </div>


        <section class="min-vh-70 o-hidden d-flex flex-column justify-content-center">
            <div class="container">
                <div class="row justify-content-center">

                </div>
            </div>
        </section>


        <div class="divider flip-x">
            <img src="/assets/img/dividers/divider-2.svg" alt="graphical divider" data-inject-svg/>
        </div>
    </div>


    <section>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-lg-12 col-xl-10">
                    <a href="#reports" class="card card-article-wide flex-md-row no-gutters hover-shadow-3d">
                        <div class="col-md-5 col-lg-6">
                            @if (trim(strtolower($device->so)) == 'ios')
                                <img src="{{ asset('/assets/img/cards/ios.png') }}" alt="Image" class="card-img-top">
                            @else
                                <img src="{{ asset('/assets/img/cards/android.png') }}" alt="Image" class="card-img-top">
                            @endif
                        </div>
                        <div class="card-body d-flex flex-column justify-content-between col-auto p-4 p-lg-5">
                            <h1 class="display-4 text-dark">{{ $device->model }} device model</h1>
                            <span>
                                    @if (trim(strtolower($device->so)) == 'ios')
                                    <div data-toggle="tooltip" title="iphone-x" class="m-1 d-inline-block">
                                        <img class="icon icon-lg bg-primary" src="/assets/img/icons/theme/devices/iphone-x.svg" alt="iphone-x icon" data-inject-svg />
                                    </div>
                                @else
                                    <div data-toggle="tooltip" title="android" class="m-1 d-inline-block">
                                        <img class="icon icon-lg bg-primary" src="/assets/img/icons/theme/devices/android.svg" alt="android icon" data-inject-svg />
                                    </div>
                                @endif
                                    <p class="lead text-dark d-inline-block">{{ $device->so }}</p>
                                </span>
                            <p class="lead mb-0 text-primary font-weight-bold">View reports</p>
                        </div>
                    </a>
                </div>
            </div>

            <a name="reports"></a>

            <div class="row justify-content-center">
                <div class="col-lg-10 col-xl-8">
                    @foreach ($device->reports as $report)
                        <a href="{{ route('report.detail', [$report->id]) }}" class="card card-body flex-row align-items-center hover-shadow-sm">
                            <div class="icon-round icon-round-lg bg-primary mx-md-4">
                                {{--<img class="icon bg-primary" src="assets/img/icons/theme/general/thunder-move.svg" alt="icon" data-inject-svg />--}}
                                <img class="icon icon-lg bg-primary" src="/assets/img/icons/theme/tools/tools.svg" alt="tools icon" data-inject-svg />
                            </div>
                            <div class="pl-4">
                                <h3 class="mb-1">Report</h3>
                                <span>Click on card to view full report.</span>
                                <div class="d-flex align-items-center mt-3">
                                    <div data-toggle="tooltip" title="marker-1" class="m-1">
                                        <img class="icon icon-lg bg-primary" src="/assets/img/icons/theme/map/marker-1.svg" alt="marker-1 icon" data-inject-svg />
                                    </div>
                                    <div class="text-small">
                                        <span class="d-block">{{ $report->ubication }}</span>
                                        <span class="text-muted">{{ $report->created_at }}</span>
                                    </div>
                                </div>
                            </div>
                        </a>
                    @endforeach
                </div>
            </div>
        </div>
    </section>
@endsection
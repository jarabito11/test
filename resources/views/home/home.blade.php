@extends('layouts.default')

@section('content')

    <div data-overlay class="bg-primary text-light o-hidden position-relative">
        <div class="position-absolute w-100 h-100 o-hidden top-0">
            <div class="decoration right bottom scale-2">
                <img class="bg-primary-2" src="/assets/img/decorations/deco-blob-2.svg" alt="deco-blob-2 decoration" data-inject-svg/>
            </div>
            <div class="decoration right bottom scale-3">
                <img class="bg-white" src="/assets/img/decorations/deco-dots-6.svg" alt="deco-dots-6 decoration" data-inject-svg/>
            </div>
            <div class="decoration top left scale-2  d-none d-md-block">
                <img class="bg-primary-3" src="/assets/img/decorations/deco-blob-1.svg" alt="deco-blob-1 decoration" data-inject-svg/>
            </div>
        </div>

        <section class="min-vh-70 o-hidden d-flex flex-column justify-content-center">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-lg-10 col-xl-8">
                        <form action="{{ route('search.token') }}" method="get">
                            {{csrf_field()}}
                            <div class="input-group input-group-lg mb-3">
                                <div class="input-group-prepend">
                                  <span class="input-group-text" id="basic-addon-1">
                                    {{--<img class="icon" src="/assets/img/icons/theme/devices/phone.svg" alt="search icon" data-inject-svg />--}}
                                    <img class="icon bg-primary" src="/assets/img/icons/theme/devices/phone.svg" alt="phone icon" data-inject-svg />
                                  </span>
                                </div>
                                <input type="text" name="token" class="form-control"  placeholder="IMEI or Token" aria-label="Search" aria-describedby="basic-addon-1" >
                                <div class="input-group-append">
                                    <button type="submit" class="input-group-text">
                                        <img src="{{ asset('/assets/img/icons/theme/general/search.svg') }}" alt="search button">
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
            @if ($errors->any())
                @foreach ($errors->all() as $error)
                    <div class="col-md-4  mx-auto">
                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                            {{ $error }}
                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                    </div>
                @endforeach
            @endif
            @if (session('no-device'))
                <div class="col-md-4 mx-auto">
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        {{ session('no-device') }}
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                            <span aria-hidden="true">&times;</span>
                        </button>
                    </div>
                </div>
            @endif
        </section>



        {{--<div class="divider flip-x">
            <img src="/assets/img/dividers/divider-2.svg" alt="graphical divider" data-inject-svg/>
        </div>--}}
    </div>

@endsection
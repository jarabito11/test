<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class ComponentsTested extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = 'components_testeds';


    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_component', 'id_report', 'value', "work"
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [];

    public function component(){
        return $this->belongsTo('App\Component', 'id_component');
    }

    public function report(){
        return $this->belongsTo('App\Report', 'id_report');
    }
}

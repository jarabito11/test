<?php

namespace App;

use Laravel\Passport\HasApiTokens;
use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Report extends Authenticatable
{
    use HasApiTokens, Notifiable;

    protected $table = 'reports';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id_user', 'id_device', 'ubication'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'created_at', 'updated_at',
    ];

    public function user(){
        return $this->belongsTo('App\User', 'id_user');
    }

    public function device(){
        return $this->belongsTo('App\Device', 'id_device');
    }

    public function componentsTesteds(){
        return $this->hasMany('App\ComponentsTested', 'id_report');
    }
}


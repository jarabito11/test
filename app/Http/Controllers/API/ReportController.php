<?php

namespace App\Http\Controllers\API;

use App\ComponentsTested;
use App\Report;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;

class ReportController extends Controller
{
    //
    public function getReports(Request $request){

        $id=$request->token; //TOKEN DEL TELEFONO

        $device=DB::table("devices")
            ->where("token", $id)
            ->orWhere("imei", $id)
            ->first();

        if ($device == null){
            return response()->json([
                'message' => "No existe ningún teléfono con estos datos."
            ], 404);
        }

        $reports=DB::table("reports")->where("id_device", $device->id)->get();

        //dd($reports);
        return response()->json([
            'reports' => $reports], 201);

    }

    //BUSCAR UN REPORTE CON UN ID
    public function getReportId(Request $request){
        $id_report=$request->id_report;

        $report=DB::table("components_testeds")->where("id_report", $id_report)->first();

        //dd($reports);
        return response()->json([
            'report' => $report], 201);
    }

    public function postNewReport(Request $request){

        //GUARDAMOS EL REPORTE
        $report=new Report();
        $report->id_user=Auth::user()->id;
        $report->ubication=$request->report['ubication'];

        $device=DB::table('devices')->where('token', $request->report['token'])->first();
        $report->id_device=$device->id;

        //dd($report);

        //APLICAR LOS DATOS GUARDADOS
        $report->save();

        //GUARDAMOS LOS COMPONENTES TESTEADOS
        foreach ($request->components as $component){
            $component_tested= new ComponentsTested();

            $component_tested->id_component=$component['id_component'];
            $component_tested->work=$component['work'];

            $component_tested->id_report=$report->id;

            if (isset($component['value'])){
                $component_tested->value=$component['value'];
            }

            $component_tested->save();

        }


        return response()->json([
            'message' => "El reporte genereado se ha guardado correctamente.",
            "title" => "Reporte guardado"
            ], 201);

    }
}

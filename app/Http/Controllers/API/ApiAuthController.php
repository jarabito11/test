<?php

namespace App\Http\Controllers;

use App\Device;
use App\User;
use Carbon\Carbon;
use DateTime;
use Firebase\Auth\Token\Exception\ExpiredToken;
use Firebase\Auth\Token\Exception\InvalidToken;
use Firebase\Auth\Token\Exception\IssuedInTheFuture;
use Firebase\Auth\Token\Verifier;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Kreait\Firebase\JWT\IdTokenVerifier;
use PhpParser\Node\Expr\Cast\Object_;

class ApiAuthController extends Controller
{

    const passForToken = "jarabito";
    const projectId = "flubber-es";

    const HEADERS = [
        'Access-Control-Allow-Origin' => '*',
        'Access-Control-Allow-Methods' => '*',
        'Content-Type' => 'application/json'
    ];



    private function checkUser($data)
    {

        $email = $data['email'];
        $user = User::where('email', $email)->first();
        if (!isset($user)) {
            //SI NO EXISTE LO CREO
            $user = new User();
            $user->name = $data['name'];
            $user->email = $email;
        } else {
            //SI EXISTE LO ACTUALIZO
            $user->name = $data['name'];
        }
        $user->save();

        return $user;

    }

    private function checkDevice($imei, $model, $so, $user_id)
    {

        $device = Device::where('imei', $imei)->first();

        if (!isset($device)) {
            $token=$this->searchTokenNotExist();

            //SI NO EXISTE LO CREO
            $device = new Device();
            $device->id_user = $user_id;
            $device->token = $token;
            $device->model = $model;
            $device->so = $so;
            $device->imei = $imei;
            $device->save();
        }else{
            $token=$device->token;
        }

        return $token;

    }

    private function searchTokenNotExist(){

        $token = rand(100000000, 999999999);

        do{
            $existe=DB::table('devices')->where('token', $token)->first();
        }while(isset($existe));

        return $token;

    }

    public function index(Request $request)
    {

        //DATOS DEL USUARIO
        $tokenGoogle = $request->token;
        $imei=$request->imei;

        if (!isset($tokenGoogle)){
            $data = array('message' => 'Token is empty.');
            return response()->json($data, 402, self::HEADERS, false);
        }elseif (!isset($imei)){
            $data = array('message' => 'IMEI is empty.');
            return response()->json($data, 402, self::HEADERS, false);
        }

        //CREAMOS LA INSTANCIA DE FIREBASE
        $verifier = IdTokenVerifier::createWithProjectId(self::projectId);
        try {
            //Verify token
            $token = $verifier->verifyIdToken($tokenGoogle);

            //GUARDAMOS EL USUARIO
            $user = $this->checkUser($token->payload());

            //GUARDAMOS EL DISPOSITIVO
            $tokenDevice=$this->checkDevice(
                $imei,
                $request->model,
                $request->so,
                $user->id);

            //CREAMOS EL TOKEN
            $tokenResult = $user->createToken(self::passForToken);
            $token = $tokenResult->token;
            $token->expires_at = Carbon::now()->addWeeks(2);
            $token->save();

            //Return response
            $response = [
                "expires_at" => $token->expires_at,
                "token" => $tokenResult->accessToken,
                "device" => $tokenDevice
            ];

            return response()->json($response, 200, self::HEADERS, false);

        } catch (ExpiredToken $e) {
            $data = array('message' => 'Firebase Token is expired');

            return response()->json($data, 402, self::HEADERS, false);

        } catch (IssuedInTheFuture $e) {
            $data = array('message' => 'Firebase Token is issued in the future');
            return response()->json($data, 403, self::HEADERS, false);

        } catch (InvalidToken $e) {
            $data = array('message' => 'Firebase Token is invalid');
            return response()->json($data, 401, self::HEADERS, false);
        } catch (Exception $e) {
            $data = array('message' => $e->getMessage());
            return response()->json($data, 500, self::HEADERS, false);
        } catch (\Throwable $e) {
            $data = array('message' => $e->getMessage());
            return response()->json($data, 500, self::HEADERS, false);
        }

    }

    public function getDevices(){
        $devices=DB::table('devices')->where("id_user", Auth::user()->id)->get();
        $data = array('devices' => $devices);
        return response()->json($data, 200, self::HEADERS, false);
    }


    public function logout(Request $request)
    {
        $request->user()->token()->revoke();
        return response()->json(['message' =>
            'Successfully logged out']);
    }

    public function user(Request $request)
    {
        return response()->json($request->user());
    }
}
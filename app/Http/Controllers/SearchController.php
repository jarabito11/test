<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Device;

class SearchController extends Controller
{
    public function search(Request $request){
        $errors = $request->validate([
            'token' => 'required|numeric'
        ]);
        $token = $request->input('token');

        $device = Device::where('imei', $token)->orWhere('token', $token)->first();


        /*var_dump($device->reports);
        die();*/


        if($device){
            return view('home.results', [
                'device' => $device
            ]);
        }else{
            return redirect('/')->with('no-device', 'No devices with that IMEI or Token');
        }

    }

}

<?php
namespace App\Http\Controllers;

use App\Report;
use App\ComponentsTested;
use Illuminate\Http\Request;

class DeviceController extends Controller
{
    public function detail($id){

        $componentTested = ComponentsTested::where('id_report', $id)->get();

        /*var_dump($componentTested->count());
        die();*/

        return view('report.detail', [
            'componentTested' => $componentTested
        ]);
    }
}

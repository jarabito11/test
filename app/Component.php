<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Component extends Model
{

    protected $table = 'components';


    public function componentsTesteds(){
        return $this->hasMany('App\ComponentsTested', 'id_component');
    }
}

<?php


use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(components::class);

        $this->call(users::class);
        $this->call(devices::class);
        $this->call(reports::class);
        $this->call(components_tested::class);
    }
}

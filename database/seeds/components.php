<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class components extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    private $ruta="/components/";
    private $table="components";
    public function run()
    {
        //
        DB::table($this->table)->insert([
            'type' => 'battery',
            'image' => $this->ruta . 'battery.png',
            'name' => "Batería",
        ]);

        DB::table($this->table)->insert([
            'type' => 'bluetooth',
            'image' => $this->ruta . 'bluetooth.png',
            'name' => "Bluetooth",
        ]);

        DB::table($this->table)->insert([
            'type' => 'camera',
            'image' => $this->ruta . 'camera.png',
            'name' => "Cámara",
        ]);

        DB::table($this->table)->insert([
            'type' => 'charge',
            'image' => $this->ruta . 'charge.png',
            'name' => "Conector de carga",
        ]);

        DB::table($this->table)->insert([
            'type' => 'cpu',
            'image' => $this->ruta . 'cpu.png',
            'name' => "CPU",
        ]);

        DB::table($this->table)->insert([
            'type' => 'fingerprint',
            'image' => $this->ruta . 'fingerprint.png',
            'name' => "Huella",
        ]);

        DB::table($this->table)->insert([
            'type' => 'flash',
            'image' => $this->ruta . 'flash.png',
            'name' => "Flash",
        ]);

        DB::table($this->table)->insert([
            'type' => 'gps',
            'image' => $this->ruta . 'gps.png',
            'name' => "GPS",
        ]);

        DB::table($this->table)->insert([
            'type' => 'gyroscope',
            'image' => $this->ruta . 'gyroscope.png',
            'name' => "Giroscopio",
        ]);

        DB::table($this->table)->insert([
            'type' => 'microphone',
            'image' => $this->ruta . 'microphone.png',
            'name' => "Micrófono",
        ]);

        DB::table($this->table)->insert([
            'type' => 'nfc',
            'image' => $this->ruta . 'nfc.png',
            'name' => "NFC",
        ]);

        DB::table($this->table)->insert([
            'type' => 'plug',
            'image' => $this->ruta . 'plug.png',
            'name' => "Carga",
        ]);

        DB::table($this->table)->insert([
            'type' => 'rooted',
            'image' => $this->ruta . 'rooted.png',
            'name' => "Root",
        ]);

        DB::table($this->table)->insert([
            'type' => 'signal',
            'image' => $this->ruta . 'signal.png',
            'name' => "Cobertura",
        ]);

        DB::table($this->table)->insert([
            'type' => 'speaker',
            'image' => $this->ruta . 'speaker.png',
            'name' => "Altavoz",
        ]);

        DB::table($this->table)->insert([
            'type' => 'storage',
            'image' => $this->ruta . 'storage.png',
            'name' => "Almacenamiento",
        ]);

        DB::table($this->table)->insert([
            'type' => 'wifi',
            'image' => $this->ruta . 'wifi.png',
            'name' => "Wi-Fi",
        ]);
    }
}

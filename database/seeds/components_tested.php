<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class components_tested extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //REPORT 1
        DB::table('components_testeds')->insert([
            'id_component' => 1,
            'id_report' => 1,
            'value' => 80,
            'work' => true,
        ]);

        DB::table('components_testeds')->insert([
            'id_component' => 2,
            'id_report' => 1,
            'work' => true,
        ]);

        DB::table('components_testeds')->insert([
            'id_component' => 4,
            'id_report' => 1,
            'work' => true,
        ]);

        DB::table('components_testeds')->insert([
            'id_component' => 5,
            'id_report' => 1,
            'value' => 60,
            'work' => true,
        ]);

        DB::table('components_testeds')->insert([
            'id_component' => 6,
            'id_report' => 1,
            'work' => false,
        ]);


        //REPORT 2
        DB::table('components_testeds')->insert([
            'id_component' => 5,
            'id_report' => 2,
            'value' => 70,
            'work' => true,
        ]);

        DB::table('components_testeds')->insert([
            'id_component' => 6,
            'id_report' => 2,
            'work' => false,
        ]);
    }
}

<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Psy\Util\Str;

class users extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => "Javi",
            'email' => 'javi@gmail.com',
            'password' => bcrypt('1234'),
        ]);

        DB::table('users')->insert([
            'name' => "Raúl",
            'email' => 'raul@gmail.com',
            'password' => bcrypt('1234'),
        ]);
        //Str::random(10)
    }
}

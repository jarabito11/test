<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class reports extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('reports')->insert([
            'id_user' => 1,
            'id_device' => 1,
            'ubication' => 'Onil, Alicante',

        ]);

        DB::table('reports')->insert([
            'id_user' => 2,
            'id_device' => 2,
            'ubication' => 'Alicante',

        ]);
    }
}

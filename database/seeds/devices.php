<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class devices extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //DISPOSITIVOS CON REPORTE
        DB::table('devices')->insert([
            'id_user' => 1,
            'token' => '12345678',
            'model' => 'iPhone X',
            'so' => 'iOS',
            'imei' => '353039095763931',

        ]);

        DB::table('devices')->insert([
            'id_user' => 2,
            'token' => '12345677',
            'model' => 'iPhone XS',
            'so' => 'iOS',
            'imei' => '353039095763932',

        ]);

        //DISPOSITIVOS SIN REPORTE
        DB::table('devices')->insert([
            'id_user' => 1,
            'token' => '12345679',
            'model' => 'Huawei P10',
            'so' => 'Android',
            'imei' => '353039095763933',

        ]);
    }
}

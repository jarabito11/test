<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class ComponentsTested extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('components_testeds', function (Blueprint $table) {
            $table->increments('id');
            $table->integer('id_component');
            $table->integer('id_report');
            $table->double('value')->nullable();
            $table->boolean('work');

            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('components_testeds');
    }
}
